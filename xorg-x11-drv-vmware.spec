%global moduledir %(pkg-config xorg-server --variable=moduledir )
%global driverdir   %{moduledir}/drivers

%undefine _hardened_build

Name:           xorg-x11-drv-vmware
Version:        13.4.0
Release:        1
Summary:        Xorg X11 vmware video driver
License:        MIT
URL:            http://www.x.org
Source0:        https://ftp.x.org/pub/individual/driver/xf86-video-vmware-%{version}.tar.xz

ExclusiveArch:  %{ix86} x86_64 ia64

BuildRequires:  autoconf automake libtool xorg-x11-server-devel >= 1.10.99.902
BuildRequires:  libdrm-devel libXext-devel libX11-devel systemd-devel
BuildRequires:  mesa-libxatracker-devel >= 8.0.1-4

Requires:       libxatracker >= 8.0.1-4 Xorg %(xserver-sdk-abi-requires ansic)
Requires:       Xorg %(xserver-sdk-abi-requires videodrv)

%description 
X.Org X11 vmware video driver.

%package_help

%prep
%autosetup -n xf86-video-vmware-%{version} -p1

%build
autoreconf -v --install || exit 1
%configure
%make_build

%install
%make_install

%delete_la_and_a

%files
%defattr(-,root,root)
%license COPYING
%{driverdir}/vmware_drv.so

%files help
%defattr(-,root,root)
%doc README ChangeLog
%{_mandir}/man4/vmware.4*

%changelog
* Sat Jan 28 2023 Chenxi Mao <chenxi.mao@suse.com> - 13.4.0-1
- Type:update
- ID:NA
- SUG:NA
- DESC: upgrade to 13.4.0

* Mon Oct 24 2022 zhouyihang <zhouyihang3@h-partners.com> - 13.3.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:vmwgfx fix missing array notation

* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 13.3.0-2
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update source url

* Wed Jul 29 2020 lunankun <lunankun@huawei.com> - 13.3.0-1
- Type:update
- ID:NA
- SUG:restart
- DESC:update to xorg-x11-drv-vmware-13.3.0

* Tue Mar 10 2020 hexiujun <hexiujun1@huawei.com> - 13.2.1-1
- Package init
